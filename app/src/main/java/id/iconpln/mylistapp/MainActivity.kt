package id.iconpln.mylistapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val listHero: ListView
        get() = lv_list_hero

    private var list: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //loadListArrayAdapter()
        loadListBaseAdapter(this)
        setListClickListener(listHero)
    }

    private fun setListClickListener(listView: ListView) {

        listView.onItemClickListener = object : AdapterView.OnItemClickListener {
                override fun onItemClick(adapter: AdapterView<*>?, view: View?, index: Int, l: Long) {
                    Toast.makeText(this@MainActivity, "${list[index].name}", Toast.LENGTH_LONG).show()
                    showDetailHero(list[index])
                }
            }
    }

    private fun showDetailHero(hero: Hero) {

        val objectIntent = Intent(this, DetailHeroActivity::class.java)
        objectIntent.putExtra(DetailHeroActivity.EXTRA_HERO, hero)
        startActivity(objectIntent)
    }
    private fun loadListBaseAdapter(context: Context) {
        list.addAll(HeroesData.listDataHero)

        val baseAdapter = ListViewHeroAdapter(context, list)
            listHero.adapter = baseAdapter
    }

    fun loadListArrayAdapter() {
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getDataHero())
        listHero.adapter = adapter
    }

    fun getDataHero(): Array<String> {
        val hero = arrayOf(
            "Cut Nyak Dien",
            "Ki Hajar Dewantara",
            "Moh Yamin",
            "Patimura",
            "R A Kartini",
            "Sukarno"
        )
        return hero
    }
}
